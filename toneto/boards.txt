menu.clock=Clock

##############################################################

atmega328p.name=ATmega328P

atmega328p.upload.tool=arduino:avrdude
atmega328p.upload.protocol=arduino
atmega328p.upload.maximum_size=30720
atmega328p.upload.maximum_data_size=2048
atmega328p.upload.speed=57600

atmega328p.bootloader.tool=arduino:avrdude
atmega328p.bootloader.unlock_bits=0x3F
atmega328p.bootloader.lock_bits=0x0F

atmega328p.build.board=atmega328p
atmega328p.build.core=arduino:arduino
atmega328p.build.variant=arduino:standard
atmega328p.build.mcu=atmega328p

atmega328p.menu.clock.internal8=8 MHz (internal)
atmega328p.menu.clock.internal8.build.f_cpu=8000000L
atmega328p.menu.clock.internal8.bootloader.low_fuses=0xE2
atmega328p.menu.clock.internal8.bootloader.high_fuses=0xDA
atmega328p.menu.clock.internal8.bootloader.extended_fuses=0xFD
atmega328p.menu.clock.internal8.bootloader.file=atmega/ATmegaBOOT_168_atmega328_pro_8MHz.hex
